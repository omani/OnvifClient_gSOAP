# OnvifClient_gSOAP

An ONVIF client base on gSOAP

## Getting Started

This project will help you develop a onvif client with C++. An IPC support onvif is needed to run the samples.

### Prerequisites

- gSOAP & ONVIF client code

  reference [GENERATE_ONVIF_CODE](./GENERATE_ONVIF_CODE.md)

- cmake 3.4.1 +

### Installing

1. modify `samples/common/common.h` , change `USERNAME` `PASSWORD` `HOSTNAME` `PROFILETOKEN` for your IPC device

2. make

   ```bash
   $ mkdir build
   $ cd build
   $ cmake .. && make
   $ make install
   ```

### Running samples

- deviceBiding

  Get device information

  ```bash
  $ ./samples/deviceBinding/deviceBinding
  
  ====================== DeviceBinding DeviceInformation ======================
  Manufacturer:    ONVIF
  Model:           DS-2DC4223IW-D
  FirmwareVersion: V5.6.11 build 190426
  SerialNumber:    DS-2DC4223IW-*********************
  HardwareId:      88
  ====================== DeviceBinding Capabilities ======================
  Analytics address: http://192.168.1.164/onvif/Analytics
  Device address:    http://192.168.1.164/onvif/device_service
  Events address:    http://192.168.1.164/onvif/Events
  Imaging address:   http://192.168.1.164/onvif/Imaging
  Media address:     http://192.168.1.164/onvif/Media
  PTZ address:       http://192.168.1.164/onvif/PTZ
  ====================== DeviceBinding Scopes ======================
  onvif://www.onvif.org/type/video_encoder
  onvif://www.onvif.org/Profile/Streaming
  onvif://www.onvif.org/Profile/G
  onvif://www.onvif.org/Profile/T
  onvif://www.onvif.org/type/ptz
  onvif://www.onvif.org/MAC/****************
  onvif://www.onvif.org/hardware/DS-2DC4223IW-D
  onvif://www.onvif.org/name/HIKVISION%20DS-2DC4223IW-D
  onvif://www.onvif.org/location/city/hangzhou
  ```

- mediaBinding

  Get profile, snapshoturi, stramuri etc..

  ```bash
  $ ./samples/mediaBinding/mediaBinding
  
  ====================== MediaBinding Profiles ======================
  Profile name         Profile token
  mainStream         Profile_1
  subStream         Profile_2
  thirdStream         Profile_3
  ====================== MediaBinding GetSnapshotUri ======================
  SnapshotUri: http://192.168.1.164/onvif-http/snapshot?Profile_1
  ====================== MediaBinding GetStreamUri ======================
  StreamUri: rtsp://192.168.1.164:554/Streaming/Channels/101?transportmode=unicast&profile=Profile_1
  ```

- ptzBinding

  ptz control, your device must support ptz control

  ```bash
  $ ./samples/ptzBinding/ptzBinding
  
  ====================== PTZBinding Configurations ======================
  NodeToken: PTZNODETOKEN
  DefaultAbsolutePantTiltPositionSpace: http://www.onvif.org/ver10/tptz/PanTiltSpaces/PositionGenericSpace
  DefaultAbsoluteZoomPositionSpace: http://www.onvif.org/ver10/tptz/ZoomSpaces/PositionGenericSpace
  DefaultRelativePanTiltTranslationSpace: http://www.onvif.org/ver10/tptz/PanTiltSpaces/TranslationGenericSpace
  DefaultRelativeZoomTranslationSpace: http://www.onvif.org/ver10/tptz/ZoomSpaces/TranslationGenericSpace
  DefaultContinuousPanTiltVelocitySpace: http://www.onvif.org/ver10/tptz/PanTiltSpaces/VelocityGenericSpace
  DefaultContinuousZoomVelocitySpace: http://www.onvif.org/ver10/tptz/ZoomSpaces/VelocityGenericSpace
  DefaultPTZSpeed Pan:     0.1
  DefaultPTZSpeed Tilt:    0.1
  DefaultPTZSpeed Zoom:    1
  DefaultPTZTimeout:       300000
  PanTiltLimits Pan Min:   -1
  PanTiltLimits Pan Max:   1
  PanTiltLimits Tilt Min:  -1
  PanTiltLimits Tilt Max:  1
  ZoomLimits Min:          0
  ZoomLimits Max:          1
  Extension:               0
  MoveRamp:                0
  PresetRamp:              0
  PresetTourRamp:          0
  ====================== PTZBinding Status ======================
  Pan:  0.303444
  Tilt: 0.4
  Zoom: 0
  MovingStatusPanTilt: 0
  MovingStatusZoom: 0
  ====================== PTZBinding AbsoluteMove ======================
  Moving
  stop
  ====================== PTZBinding Status ======================
  Pan:  0.3
  Tilt: 0.4
  Zoom: 0
  MovingStatusPanTilt: 0
  MovingStatusZoom: 0
  ====================== PTZBinding ContinuousMove ======================
  Moving
  stop
  ====================== PTZBinding Status ======================
  Pan:  0.303778
  Tilt: 0.4
  Zoom: 0
  MovingStatusPanTilt: 0
  MovingStatusZoom: 0
  ```
  
# gsoap工具生成相关文件
1. 下载gsoap https://sourceforge.net/projects/gsoap2/ 当前version为2.8
   1. 解压得到目录gsoap-2.8
2. 新建gsoapwork目录
   1. 把gsoap-2.8/gsoap目录下的plugin,import,custom三个目录都拷贝到gsoapwork目录下
   2. 把gsoap-2.8/gsoap/bin/win32目录下wsdl2h.exe,soapcpp2.exe拷贝到gsoapwork目录下
   3. 把gsoap-2.8/gsoap目录下的stdsoap2.cpp,stdsoap2.h,dom.cpp,typemap.dat拷贝到gsoapwork目录下
3. 下载wsdl https://www.onvif.org/profiles/specifications/
    1. 在gsoapwork目录下新建目录ver10
    2. 下载onvif.xsd,common.xsd到ver10/schema
    3. 下载types.xsd到ver10/pacs
4. 功能wsdl文件链接（可选,gsoap目录下新建wsdl并将需要的链接存到wsdl目录下）
    1. http://www.onvif.org/ver10/device/wsdl/devicemgmt.wsdl
    2. http://www.onvif.org/ver10/media/wsdl/media.wsdl
    3. http://www.onvif.org/ver20/ptz/wsdl/ptz.wsdl
    4. http://www.onvif.org/onvif/ver10/replay.wsdl
    5. http://www.onvif.org/onvif/ver10/deviceio.wsdl
    6. http://www.onvif.org/onvif/ver10/network/wsdl/remotediscovery.wsdl
    7. http://www.onvif.org/onvif/ver20/imaging/wsdl/imaging.wsdl 
    8. http://www.onvif.org/onvif/ver10/search.wsdl
    9. http://www.onvif.org/onvif/ver10/recording.wsdl
5. 
    1. 防止出现LONG64的错误和‘SOAP_TYPE_xsd__duration’ was not declared in this scope错误,需要在gsoapwork/typemap.dat中
    ```bash
    # xsd__duration = #import "custom/duration.h" | xsd__duration
    删掉#号为
    xsd__duration = #import "custom/duration.h" | xsd__duration"
    ```
   2. 防止SOAP_ENV__Fault重复错误,修改gsoapwork/import/wsa5.h修改SOAP_ENV__Fault为SOAP_ENV__Fault_alex
6. 在gsoapwork目录执行生成onvif
```bash
wsdl2h -x -t typemap.dat -o onvif.h .\wsdl\remotediscovery.wsdl .\wsdl\devicemgmt.wsdl .\wsdl\media.wsdl .\wsdl\ptz.wsdl .\wsdl\replay.wsdl 1.\wsdl\search.wsdl http://www.onvif.org/onvif/ver10/display.wsdl .\wsdl\deviceio.wsdl http://www.onvif.org/onvif/ver10/event/wsdl/event.wsdl http://www.onvif.org/onvif/ver10/receiver.wsdl .\wsdl\recording.wsdl
```
7. 得到onvif.h后在头文件添加
```bash 
#import "wsse.h"
```
8. 在gsoapwork目录执行,生成onvif开发框架代码
```bash 
soapcpp2 -2 -x -C onvif.h  -L -Iimport
```
9. 把gsoapwork目录下 
   soapC.cpp
   soapClient.cpp
   soapH.h
   soapStub.h
   wsdd.nsmap
   dom.c
   stdsoap2.cpp
   stdsoap2.h
   import/dom.h
   import/wsa5.h
   custom/duration.c
   custom/duration.h
   custom/struct_timeval.h
   custom/struct_timeval.c
   plugin/mecevp.c
   plugin/mecevp.h
   plugin/smdevp.c
   plugin/smdevp.h
   plugin/threads.c
   plugin/threads.h
   plugin/wsaapi.c
   plugin/wsaapi.h
   plugin/wsseapi.cpp
   plugin/wsseapi.h
   拷贝到对应目录下并且.c文件全改为.cpp文件
   1. stdsoap2.cpp头文件添加
   ```bash
   #include "wsdd.nsmap"
   ```
10. 生成代理头文件和类,在gsoapwork目录新建onvif
```bash
soapcpp2 -2 -x -C -c++ -j -d onvif onvif.h  -L -Iimport
```


   

#

- remoteDiscovery

  Find  devices

  **NOT FINISH**

  